﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CBuyConfirmUI : MonoBehaviour {

    public static CBuyConfirmUI s_Instance;

    public Text _txtCost;
    public Text _txtCostType;
    public Text _txtGain;

    public uint m_nGainNum = 0;
    public int m_nGainType;

    public string m_szProductId = "";

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetCostType( string szType )
    {
        _txtCostType.text = szType;
    }

    public void SetCostNum( string szNum )
    {
        _txtCost.text = szNum;
    }

    public void SetGain( string szGain )
    {
        _txtGain.text = szGain;
    }

    public void SetProductId( string szProductId )
    {
        m_szProductId = szProductId;
    }

    public string GetProductId()
    {
        return m_szProductId;
    }
}
