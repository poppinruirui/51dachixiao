﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CMsgBox : MonoBehaviour {

    public static CMsgBox s_Instance = null;

    public float m_fTotalShowTime = 2f;
    float m_fCurShowTimeLapse = 0f;

    public GameObject _goContainer;
    
    public Text _txtContent;

    void Awake()
    {
        s_Instance = this;
    }
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        ShowLoop();
    }

    public void Show( string szContent )
    {
        _txtContent.text = szContent;
        m_fCurShowTimeLapse = m_fTotalShowTime;
        _goContainer.gameObject.SetActive( true );
    }

    void ShowLoop()
    {
        if (m_fCurShowTimeLapse <= 0f)
        {
            return;
        }

        m_fCurShowTimeLapse -= Time.deltaTime;
        if (m_fCurShowTimeLapse <= 0f)
        {
            _goContainer.gameObject.SetActive(false);
        }
    }
}
