﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;

public class CGrassEditor : MonoBehaviour {

	public static CGrassEditor s_Instance;

	static Vector3 vecTempScale = new Vector3();
	static Vector3 vecTempPos = new Vector3();

	public GameObject m_goGrassContainer;

	/// ! -- grass ui
	public Dropdown _dropdownGrassID;
	public Dropdown _dropdownGrassType;
	public InputField _inputPolygonId;
	public InputField _inputColor;
	const int c_nValueNum = 2;
	public InputField[] _aryInputValue = new InputField[c_nValueNum];
	public InputField[] _aryInputDesc = new InputField[c_nValueNum];
	public Button _btnAddGrass;


	// 草丛功能
	public enum eGrassFunc
	{
		none,  // (----)
		speed, // 影响速度
		shell, // 消除壳
	};

	public struct sGrassConfig
	{
		public int nId;
		public int nFunc;
		public int nPolygonId;
		public string szColor;
		public float[] aryValue;
		public string[] aryDesc;
	};
	sGrassConfig m_CurGrassConfig;
	Dictionary<int, sGrassConfig> m_dicGrassConfig = new Dictionary<int, sGrassConfig> ();

	public struct sGrassInfo
	{
		public int nConfigId;
		public float fScaleX;
		public float fScaleY;
		public float fRotation;
	};

	void Awake()
	{
		s_Instance = this;
	}

	// Use this for initialization
	void Start () {
		InitDropdown_GrassID ();
		InitDropdown_GrassType ();
		m_CurGrassConfig = GetGrassConfigById (0);
	}
	
	// Update is called once per frame
	void Update () {

	}

	sGrassConfig tempGrassConfig;
	public sGrassConfig GetGrassConfigById( int nId )
	{
		if (!m_dicGrassConfig.TryGetValue (nId, out tempGrassConfig)) {
			tempGrassConfig = new sGrassConfig ();
			tempGrassConfig.nId = nId;
			tempGrassConfig.aryValue = new float[c_nValueNum];
			tempGrassConfig.aryDesc = new string[c_nValueNum];
			m_dicGrassConfig [nId] = tempGrassConfig;
		}
		return tempGrassConfig;
	}
		
	public void InitDropdown_GrassID()
	{
		List<string> showNames = new List<string>();

		showNames.Add( "0");
		showNames.Add( "1");
		showNames.Add( "2");
		showNames.Add( "3");
		showNames.Add( "4");
		showNames.Add( "5");
		showNames.Add( "6");
		showNames.Add( "7");
		showNames.Add( "8");
		UIManager.UpdateDropdownView( _dropdownGrassID, showNames);
	}

	public void InitDropdown_GrassType()
	{
		List<string> showNames = new List<string>();

		showNames.Add( "（选择草丛功能）");
		showNames.Add( "速度加成");
		showNames.Add( "消除壳");

		UIManager.UpdateDropdownView( _dropdownGrassType, showNames);
	}

	public void OnClickBtn_AddGrass()
	{
		CGrass grass = ResourceManager.s_Instance.ReuseGrass();
        vecTempPos.x = Screen.width / 2;
        vecTempPos.y = Screen.height / 2;
        vecTempPos = Camera.main.ScreenToWorldPoint(vecTempPos);
        grass.SetPos(vecTempPos);
        AddToGrassContainer ( grass );
		grass.SetScale ( 30, 30 );
		grass.SetColor ( m_CurGrassConfig.szColor );
		grass.SetConfigId ( m_CurGrassConfig.nId );
	}

	void AddToGrassContainer( CGrass grass )
	{
		grass.transform.parent = m_goGrassContainer.transform;
	}

	public void OnDropdown_GrassType()
	{
		m_CurGrassConfig.nFunc = _dropdownGrassType.value;
		m_dicGrassConfig [m_CurGrassConfig.nId] = m_CurGrassConfig;
	}

	public void OnDropdown_GrassID()
	{
		m_CurGrassConfig = GetGrassConfigById ( _dropdownGrassID.value );
		UpdateUiContent ();
	}

	void UpdateUiContent()
	{
		_dropdownGrassType.value = m_CurGrassConfig.nFunc;
		_inputColor.text = m_CurGrassConfig.szColor;
		for (int i = 0; i < c_nValueNum; i++) {
			_aryInputValue [i].text = m_CurGrassConfig.aryValue [i].ToString ();
			_aryInputDesc [i].text = m_CurGrassConfig.aryDesc [i];
		}

	}

	public void OnInputValueChanged_PolygonId()
	{
		m_CurGrassConfig.nPolygonId = int.Parse ( _inputPolygonId.text );
		m_dicGrassConfig [m_CurGrassConfig.nId] = m_CurGrassConfig;
	}

	public void OnInputValueChanged_Color()
	{
		m_CurGrassConfig.szColor = _inputColor.text;
		m_dicGrassConfig [m_CurGrassConfig.nId] = m_CurGrassConfig;
	}

	public void OnInputValueChanged_Value0()
	{
		m_CurGrassConfig.aryValue [0] = float.Parse ( _aryInputValue[0].text );
		m_dicGrassConfig [m_CurGrassConfig.nId] = m_CurGrassConfig;
	}

	public void OnInputValueChanged_Desc0()
	{
		m_CurGrassConfig.aryDesc [0] =  _aryInputDesc[0].text ;
		m_dicGrassConfig [m_CurGrassConfig.nId] = m_CurGrassConfig;
	}

	public void OnInputValueChanged_Value1()
	{
		m_CurGrassConfig.aryValue [1] = float.Parse ( _aryInputValue[1].text );
		m_dicGrassConfig [m_CurGrassConfig.nId] = m_CurGrassConfig;
	}

	public void OnInputValueChanged_Desc1()
	{	m_CurGrassConfig.aryDesc [1] =  _aryInputDesc[1].text ;
		m_dicGrassConfig [m_CurGrassConfig.nId] = m_CurGrassConfig;

	}

	void SaveGrassInstance ( XmlDocument xmlDoc, XmlNode node )
	{
		int nGuid = 0;
		foreach (Transform child in m_goGrassContainer.transform) {
			CGrass grass = child.gameObject.GetComponent<CGrass> ();
			XmlNode grass_instance_node = StringManager.CreateNode (xmlDoc, node, "G" + nGuid ); 
			nGuid++;
			StringManager.CreateNode (xmlDoc, grass_instance_node, "ConfigId", grass.GetConfigId().ToString() );
			StringManager.CreateNode (xmlDoc, grass_instance_node, "PosX", grass.GetPos().x.ToString( "f1" ) );
			StringManager.CreateNode (xmlDoc, grass_instance_node, "PosY", grass.GetPos().y.ToString( "f1" ) );
			StringManager.CreateNode (xmlDoc, grass_instance_node, "ScaleX", grass.GetScale ().x.ToString( "f1" ) );
			StringManager.CreateNode (xmlDoc, grass_instance_node, "ScaleY", grass.GetScale ().y.ToString( "f1" ) );
			StringManager.CreateNode (xmlDoc, grass_instance_node, "Rota", grass.GetRotation ().ToString( "f1" ) );
		} // end foreach
	}

	void GenerateGrassInstance (  XmlNode node )
	{
		for (int i = 0; i < node.ChildNodes.Count; i++) {
			XmlNode ins_node = node.ChildNodes [i];
			CGrass grass = ResourceManager.s_Instance.ReuseGrass();
			AddToGrassContainer ( grass );
			vecTempPos.z = -400f;
			vecTempScale.z = 1f;
			for (int j = 0; j < ins_node.ChildNodes.Count; j++) {
				XmlNode param_node = ins_node.ChildNodes[j];
				switch(param_node.Name)
				{
				case "ConfigId":
					{
						int nConfigId = int.Parse ( param_node.InnerText );
						grass.SetConfigId ( nConfigId );
						sGrassConfig grassConfig = GetGrassConfigById ( nConfigId );
						grass.SetConfig ( grassConfig );
						grass.SetColor ( grassConfig.szColor );
					}
					break;
				case "PosX":
					{
						vecTempPos.x = float.Parse ( param_node.InnerText );
					}
					break;
				case "PosY":
					{
						vecTempPos.y = float.Parse ( param_node.InnerText );
					}
					break;		
				case "ScaleX":
					{
						vecTempScale.x = float.Parse ( param_node.InnerText );
					}
					break;
				case "ScaleY":
					{
						vecTempScale.y = float.Parse ( param_node.InnerText );
					}
					break;
				case "Rota":
					{
						float fRot = float.Parse ( param_node.InnerText );
						grass.SetRotation ( fRot );
					}
					break;
				} // end switch
			} // end for j
			grass.SetPos( vecTempPos );
			grass.SetScale ( vecTempScale );
		} // end for i
	}

	public void SaveGrass( XmlDocument xmlDoc, XmlNode node )
	{
		foreach (KeyValuePair<int, sGrassConfig> pair in m_dicGrassConfig ) {
			if (pair.Value.nFunc == 0) {
				continue;
			}
			tempGrassConfig = pair.Value;
			XmlNode grass_node = StringManager.CreateNode (xmlDoc, node, "G" + pair.Key ); 
			StringManager.CreateNode (xmlDoc, grass_node, "Func", pair.Value.nFunc.ToString());
			StringManager.CreateNode (xmlDoc, grass_node, "PloygonId", pair.Value.nPolygonId.ToString());
			StringManager.CreateNode (xmlDoc, grass_node, "Color", pair.Value.szColor);
			for (int i = 0; i < c_nValueNum; i++) {
				StringManager.CreateNode (xmlDoc, grass_node, "Value" + i, pair.Value.aryValue[i].ToString ("f1"));
				StringManager.CreateNode (xmlDoc, grass_node, "Desc" + i, pair.Value.aryDesc[i]);
			}
		} // end foreach

		XmlNode instance_node = StringManager.CreateNode (xmlDoc, node, "Instance" ); 
		SaveGrassInstance ( xmlDoc, instance_node );

	}

	public void GenerateGrass( XmlNode node )
	{
		if (node == null || node.ChildNodes.Count == 0) {
			return;
		}

		for (int i = 0; i < node.ChildNodes.Count; i++) {
			XmlNode node_grass = node.ChildNodes [i];

			if (node_grass.Name == "Instance") {
				GenerateGrassInstance ( node_grass );
				continue;
			}

			int nGrassId = int.Parse( node_grass.Name.Substring ( 1, node_grass.Name.Length - 1 ) );
			tempGrassConfig = GetGrassConfigById ( nGrassId );
			tempGrassConfig.nId = nGrassId;
			for (int j = 0; j < node_grass.ChildNodes.Count; j++) {
				XmlNode node_param = node_grass.ChildNodes [j];
				switch (node_param.Name) {
				case "Func":
					{
						tempGrassConfig.nFunc = int.Parse ( node_param.InnerText );
					}
					break;

				case "PloygonId":
					{
						tempGrassConfig.nPolygonId = int.Parse ( node_param.InnerText );
					}
					break;
				case "Color":
					{
						tempGrassConfig.szColor = node_param.InnerText;
					}
					break;
				case "Value0":
				case "Value1":
				case "Value2":
				case "Value3":
				case "Value4":
				case "Value5":
				case "Value6":
				case "Value7":
					{
						int nIndex = int.Parse(node_param.Name.Substring( 5, node_param.Name.Length - 5 ));
						tempGrassConfig.aryValue [nIndex] = float.Parse ( node_param.InnerText );
					}
					break;
				case "Desc0":
				case "Desc1":
				case "Desc2":
				case "Desc3":
				case "Desc4":
				case "Desc5":
				case "Desc6":
				case "Desc7":
					{
						int nIndex = int.Parse(node_param.Name.Substring( 4, node_param.Name.Length - 4 ));
						tempGrassConfig.aryDesc [nIndex] = node_param.InnerText;
					}
					break;
				}
			}// end for j
			m_dicGrassConfig[tempGrassConfig.nId] = tempGrassConfig;

		} // end for i
		m_CurGrassConfig = GetGrassConfigById(0);
		UpdateUiContent ();
	}
}
